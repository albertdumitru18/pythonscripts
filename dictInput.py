# Write a program that uses a loop to take n key-value inputs from the user and create a dictionary using these keys and values.

my_dict = {}
n=int(input("Number of entries: "))

for i in range(0, n):
    key = input("Enter a key: ")
    value = input("Enter a value: ")

    my_dict[key] = value
print(my_dict)    