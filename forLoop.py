# For every non-negative integer in interval (0, n), print i^2.
# Condition (1 <= n <= 50)



n = int(input())

for i in range(n):
    if n < 1:
        print("Invalid number")
        break
    elif n > 50:
        print("Invalid number")
        break
    else:    
        print(str(i ** i))
