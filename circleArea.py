# Write a program to compute the area of a circle using a function.
pi = 3.14

def compute_area(radius):
    area = pi * radius **2
    return area

radius = float(input("Enter the radius of circle: "))


print(compute_area(radius)) 
