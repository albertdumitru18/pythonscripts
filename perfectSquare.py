# Find if a number is perfect square.
import math

number = int(input("Enter your number here: "))

sq_root = math.sqrt(number)

if(sq_root) ** 2 == number:
    print(f"{number} is perfect square!")
else:
    print(f"{number} is not perfect square!")    

