# Write a program to calculate the product of list items.

numbers = [3, 7, 31, 71, 22, 49]
product = 1
for i in numbers:
    product = product * i

print(product)