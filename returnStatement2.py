# Create a function to compute the factorial of a number.
# The number must be entered by the user.
# Use return statement.

def compute_factorial(n):

    factorial = 1

    for i in range(1, n + 1):
        factorial = factorial * i
    return factorial

n = int(input("Enter your number here: "))      

total = compute_factorial(n)
print(total)