# Make a dictionary (prices) and assign two key: value pairs of fruits and prices.
# Change then value of the first item, and then print the dictionary.
# Add another item and print the dictionary.

prices = {'apple': 2.5, 'kiwi': 3.4}

prices['apple'] = 3.5
print(prices)

prices['banana'] = 3.0
print(prices)
