# Make 3 different classes to calculate perimeter of 2 polygons.
# Use inheritance.

class Polygon:
    def __init__(self, sides):
        self.sides = sides

    def perimeter(self):
        value = 0
        for side in self.sides:
            value += side
        return value

    def informations(self):
        print("A polygon is a two dimensional shape composed from straight lines.")

class Triangle(Polygon):
    def info(self):
        print("A triangle is a two dimensional shape with 3 straight sides.")

class Quadrilater(Polygon):
    def info(self):
        print("A quadrilater is a two dimensional shape with 4 straight sides.")


triangle1 = Triangle([3, 4, 5])
quadrilater1 = Quadrilater([3, 4, 5, 6])

t1_per = triangle1.perimeter()
q1_per = quadrilater1.perimeter()
print(f"Triange perimeter: {t1_per}")
print(f"Quadrilater perimeter: {q1_per}")