# Without using any string methods, try to print the following: 123...n
# Note that "..." represents the consecutive values in between.
# Condition: 1 <= n <= 150


n = int(input("Enter your number here: "))



for i in range(n):
    if n < 1:
        print("Invalid number!")
        break
    elif n > 150:
        print("Invalid number!")
        break
    else:
        print(i + 1, end="")

