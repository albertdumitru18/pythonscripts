# Create a program to print the occurrence of a character in the string.

string1 = input("Enter a string of characters here: ")
char1 = input("Enter the character you are looking for: ")
count = 0
for i in string1:
    if i == char1:
        count += 1

print(f"There are {count} {char1} in {string1}")        