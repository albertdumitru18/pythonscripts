# Calculate average score of 7 students and then assing them a class by following:
# A ( >= 80), B (60, 80), C (50, 60), F ( <= 50)

def group_average(note):
    
    sum_notes = sum(note)
    number_students = len(note)

    group_avg = sum_notes / number_students
    return group_avg


def grades(avg):
    if avg >= 80:
        avg_class = "A"
    elif avg >= 60:
        avg_class = "B"
    elif avg >= 50:
        avg_class = "C" 
    else:
        avg_class = "F"
    return avg_class

students_notes = 55, 71, 83, 69, 95, 86, 34
final_avg = group_average(students_notes)
final_class = grades(final_avg)

print("Average score of class is: ", final_avg)
print("Class grade is: ", final_class)