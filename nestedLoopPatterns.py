# Create a diamond patters using nested for loops.







n = 5
# First half
for a in range(n):
    for b in range((n-a)+1):
        print(" ", end=" ")
    for f in range(n-b):
        print("$", end=" ") 
    for p in range((n-1)-b):
        print("$", end=" ")  
    print()    
# Second half
for i in range(n):
    for j in range(i+1):
        print(" ", end=" ")
    for k in range(n - i):
        print("$", end=" ")
    for z in range((n-1)-i):
        print("$", end=" ")    
    print()    