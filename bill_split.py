# Create a program to split the restaurant bill among friends.
# Total bill have 20% tax that is added to the consumption.


total_friends = int(input("Enter number of friends: "))
bill_amount = int(input("Enter the amount of the bill: "))

tax_amount = (20/100) * bill_amount
total_bill = bill_amount + tax_amount

pay_per_friend = total_bill / total_friends

print("Total bill for 1 friends is: ", str(pay_per_friend))