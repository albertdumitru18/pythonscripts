# Create a dict using dict comprehension.
# Key must be from 1 to n(included) and values 10 x key.
# Key >= 3

n = int(input("Enter number here: "))
key = 1
numbers = {key: key *10 for key in range(1, n+1) if key >= 3}

print(numbers)

