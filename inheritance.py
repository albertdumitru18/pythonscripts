# Create a class that inherits base class and prove the result.

class Animals:
    def eat(self):
        return "I can eat"

class Dog(Animals):
    def bark(self):
        return "I can bark"

class Cat(Animals):
    def meow(self):
        return "I can meow"

cat = Cat()
dog = Dog()

print(f"{dog.eat()} \n{dog.bark()}\n")
print(f"{cat.eat()} \n{cat.meow()}")
