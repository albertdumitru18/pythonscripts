# Make a function to return product of two numbers entered by the user.
# Use return statement.

def get_product(number1, number2):
    product = number1 * number2
    return product

n1 = int(input("Enter first number here: "))
n2 = int(input("Enter second number here: "))

total = get_product(n1, n2)
print(total)