# find even and uneven number in an array from 1 to 100

lower = 1
upper = 100


for num in range(lower, upper + 1):
   if (num % 2) == 0:
       print(str(num) + " is even number")
   else:
       print(str(num) + " is uneven number")