# Create a program to calculate the sum of integers entered by the user. 
# If the user enters 0 or negative integer, terminate the loop and print the sum.
# The negative number shouldn't be added to the total variable.

total = 0

while True:
    n = int(input())
    if n <= 0:
        break
    total = total + n
print(total)    