# Calculate profit/ loss for a store if with cost and selling price entered by the user.

cost_price = int(input("Enter product cost price: "))
selling_price = int(input("Enter product selling price: "))

if cost_price > selling_price:
    loss = selling_price - cost_price
    print(f"Loss is: {loss}")
elif cost_price == selling_price:
    print("You did not make any profit!")
else:
    profit = selling_price - cost_price
    print(f"Profit is: {profit}")        

