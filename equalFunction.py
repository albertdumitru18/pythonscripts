# Write a program to check if the two arguments passed are equal or not by creating a function.

num1 = int(input("Enter first number: "))
num2 = int(input("Enter second number: "))

def equal(num1, num2):
    if num1 == num2:
        return f"The numbers {num1} and {num2} are equal!"
    else:
        return f"The numbers {num1} and {num2} are not equal!" 
        
print(equal(num1, num2))