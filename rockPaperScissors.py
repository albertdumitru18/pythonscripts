import random

def get_user_input():
    user_input = input("Select rock/paper/scissors: ")
    user_input = user_input.lower()
    return user_input

def get_computer_input():
    option = {1: "rock", 2: "paper", 3: "scissors"}

    random_number = random.randint(1, 3)
    computer_choice = option[random_number]

    return computer_choice

def check_answear(user_input, computer_choice):
    if user_input == computer_choice:
        return "draw"
    elif user_input == "rock" and computer_choice == "scissors":
        return "win"
    elif user_input == "paper" and computer_choice == "rock":
        return "win"
    elif user_input == "scissors" and computer_choice == "paper":
        return "win"
    else:
        return "lose"    


computer_ch = get_computer_input()

while True:
    user_ch = get_user_input()
    if user_ch in ["rock", "paper", "scissors"]:
        break

result = check_answear(user_ch, computer_ch)                    

print(f"User's choice: {user_ch}")
print(f"Computer choice: {computer_ch}")
print(f"Result: {result}")