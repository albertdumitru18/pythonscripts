# Write a program to find standard deviation using the stdev().

import statistics

data_list = [2, 4, 6, 8, 10]

deviation = statistics.stdev(data_list)

print(deviation)