# Create a class named Person.
# Inside this class, create the __init__() method that takes three arguments: self and name and age.
# Inside the __init__() method, create two attributes named current_name and current_age.
# Then, assign the name argument to the current_name attribute, and the age argument to the current_age attribute.

class Person:
     def __init__(self, name, age):
        self.current_name = name
        self.current_age = age

name = input("Enter your name: ")
age = input("Enter your age: ")

person1 = Person(name, age)

print(f"Your name is {person1.current_name}")
print(f"You are {person1.current_age} years old")


