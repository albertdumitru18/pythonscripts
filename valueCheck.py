# Write a program that checks if a value (entered by the user) is in a dictionary or not.


my_dict = {"a": "juice", "b": "grill", "c": "corn"}

data = input("Enter the value: ")

flag = False

for value in my_dict.values():
    if value == data:
        flag = True
        break
if flag == True:
    print("Value found")
else:
    print("Value not found")        