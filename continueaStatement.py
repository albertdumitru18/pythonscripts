# Create a program to print odd numbers between 1 and n, where n is a integer entered by the user.
n = int(input())

for i in range(1, n +1):

    if i % 2 != 1:
        continue

    print(i)

