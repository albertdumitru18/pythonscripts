# Create a program to find the multiplication table of an integer (entered by the user) from 6 to 9.

n = int(input())

for i in range(6, 10):
    product = i * n
    print(str(n), "times", str(i), "is", product)