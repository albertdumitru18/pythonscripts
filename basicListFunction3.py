# Create a list of fruits and if a item is in the list print item "is tasty".

fruits = ["apple", "banana", "watermelon"]

if "apple" in fruits:
    print("Apple is tasty")
    