# Create a nested loop. The outer loop should iterate n times( n = input from the user) and in each iteration the outer loop iterate the inner loop 2 times.
# Inside the inner loop print "*".

n = int(input())

for i in range(n):
    for j in range(2):
        print("*")
  
